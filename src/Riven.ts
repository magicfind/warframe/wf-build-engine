import {RivenSlotData} from "./Build";
import {
	BaseUpgradeData,
	Item,
	PackagePath,
	RivenData,
	RivenUpgradeEntry,
	UpgradeData,
} from "./interfaces";

export type NormalizedRivenStats = [number, number];

const GetRivenBaseBuffValue = (
	disposition: number,
	numBuffs: number,
	hasCurse: boolean,
	isCurse: boolean,
) => {
	let baseBuffValue = disposition * 15;
	if (isCurse) {
		if (numBuffs === 2) {
			baseBuffValue *= 0.33;
		} else {
			baseBuffValue *= 0.5;
		}
	} else {
		if (hasCurse) {
			baseBuffValue *= 1.25;
		}
		if (numBuffs === 2) {
			baseBuffValue *= 0.66;
		} else {
			baseBuffValue *= 0.5;
		}
	}

	return baseBuffValue;
};

const GetDisplayAsPercent = (schema: BaseUpgradeData) =>
	!!schema.DisplayAsPercent ||
	(schema.LocKeyWordScript &&
		schema.LocKeyWordScript._displayAsPercent &&
		schema.LocKeyWordScript._displayAsPercent.length &&
		schema.LocKeyWordScript._displayAsPercent[0] === 1);

export default class Riven {
	public static isRiven(path: PackagePath) {
		return path.startsWith("/Lotus/Upgrades/Mods/Randomized/");
	}

	public static getUpgradeById(
		riven: Item<RivenData>,
		id: number,
	): RivenUpgradeEntry | null {
		return riven.data.UpgradeEntries.filter((val) => val.__id === id)[0];
	}

	public static getBaseBuffValue = (
		disposition: number,
		numBuffs: number,
		hasCurse: boolean,
		isCurse: boolean,
	) => {
		let ret = disposition * 15;
		if (isCurse) {
			if (numBuffs === 2) {
				ret *= 0.33;
			} else {
				ret *= 0.5;
			}
		} else {
			if (hasCurse) {
				ret *= 1.25;
			}
			if (numBuffs === 2) {
				ret *= 0.66;
			} else {
				ret *= 0.5;
			}
		}

		return ret;
	};

	public static getUpgradeData = (
		riven: Item<RivenData>,
		buffId: number,
		buffNormalValue: number,
		disposition: number,
		numBuffs: number,
		hasCurse: boolean,
		isCurse: boolean,
	) => {
		const baseVal = Riven.getBaseBuffValue(disposition, numBuffs, hasCurse, isCurse);
		const upgradeEntry = Riven.getUpgradeById(riven, buffId);
		if (!upgradeEntry) {
			return null;
		}

		let buffMultiplier = upgradeEntry.Upgrades[0].Upgrade.Value;
		if (isCurse) {
			buffMultiplier *= -1;
		}
		const buffRange = baseVal * buffMultiplier * 0.2;
		const minBuffVal = baseVal * buffMultiplier * 0.9;
		const displayedBuffValue = minBuffVal + buffNormalValue * buffRange;

		// return the upgrade entry with an UNRANKED value
		const upgrade: UpgradeData = {
			...upgradeEntry.Upgrades[0].Upgrade,
			Value: displayedBuffValue,
		};
		return upgrade;
	};

	public static *getPassiveUpgrades(
		riven: Item<RivenData>,
		rivenData: RivenSlotData,
		disposition: number,
	) {
		if (!rivenData.buffs) {
			return;
		}

		if (rivenData.buffs) {
			const numBuffs = rivenData.buffs.length;
			const hasCurse = !!rivenData.curse;
			for (const [buffId, buffNormalValue] of rivenData.buffs) {
				const upgrade = Riven.getUpgradeData(
					riven,
					buffId,
					buffNormalValue,
					disposition,
					numBuffs,
					hasCurse,
					false,
				);
				if (upgrade) {
					yield upgrade;
				}
			}

			if (hasCurse) {
				const [curseId, curseNormalValue] = rivenData.curse as [number, number];
				const curse = Riven.getUpgradeData(
					riven,
					curseId,
					curseNormalValue,
					disposition,
					numBuffs,
					hasCurse,
					true,
				);
				if (curse) {
					yield curse;
				}
			}
		}
	}

	public static GetAvailableRivenBuffs = (riven: Item<RivenData>) => {
		// todo: Limit this to a hard-coded set of buffs and curses
		return riven.data.UpgradeEntries.filter((rivenUpgrade) => {
			// Never roll faction damage
			if (
				rivenUpgrade.Upgrades.find(
					(upgrade) => upgrade.Upgrade.UpgradeType === "GAMEPLAY_FACTION_DAMAGE",
				)
			) {
				return false;
			}
			return rivenUpgrade.CanBeBuff === 1; //&& rivenUpgrade.CanBeCurse === 1;
		});
	};

	public static GetAvailableRivenCurses = (riven: Item<RivenData>) => {
		return riven.data.UpgradeEntries.filter((rivenUpgrade) => {
			if (
				// Never roll faction damage
				rivenUpgrade.Upgrades.find(
					(upgrade) => upgrade.Upgrade.UpgradeType === "GAMEPLAY_FACTION_DAMAGE",
				)
			) {
				return false;
			}
			return rivenUpgrade.CanBeCurse === 1;
		});
	};

	/**
	 * Converts a buff value from the input riven to a value between 0 and 1.
	 * Returns `[buffId, normalizedBuffValue, isCurse]`
	 * @param riven Riven entry from the Riven DB
	 * @param disposition Disposition of the weapon (OmegaAttenuation)
	 * @param buffVal The displayed number for this buff on the riven
	 * @param numBuffs Total number of buffs on the riven
	 * @param modRank The current rank of the riven
	 * @param hasCurse Whether the riven has a curse
	 * @param isCurse Whether the value should be treated as a curse
	 */
	public static GetNormalizedBuffValue = (
		riven: Item<RivenData>,
		disposition: number,
		buffId: number,
		buffVal: number,
		numBuffs: number,
		modRank: number,
		hasCurse: boolean,
		isCurse: boolean,
	): NormalizedRivenStats | null => {
		// if (!(numBuffs === 2 || numBuffs === 3)) return [buffId, -1, isCurse] // ..invalid buff count
		const baseBuffVal = GetRivenBaseBuffValue(disposition, numBuffs, hasCurse, isCurse);
		const normalizedBuffValue = riven.data.UpgradeEntries.reduce(
			(result, rivenUpgrade): number => {
				if (rivenUpgrade.__id === buffId) {
					const upgrade = rivenUpgrade.Upgrades[0];
					if (!upgrade) {
						// Invalid data!
						return 1;
					}
					let buffMultiplier = Math.abs(upgrade.Upgrade.Value);
					if (GetDisplayAsPercent(upgrade.Schema)) {
						buffMultiplier *= 100;
					}
					let buffRange = baseBuffVal * buffMultiplier * 0.2;
					let minBuffVal = baseBuffVal * buffMultiplier * 0.9;

					// Subtract 1 from the value if it's a multiplier (e.g. x1.5 Damage to Corpus -> 0.5)
					if (upgrade.Schema.DisplayAsMultiplier === 1) {
						buffVal -= 1;
					}
					const unrankedBuffValue = Math.abs(buffVal / (modRank + 1));
					const normalizedBuffValue = Math.min(
						1,
						Math.max(0, (unrankedBuffValue - minBuffVal) / buffRange),
					);

					result = normalizedBuffValue;
				}
				return result;
			},
			-1,
		);
		if (normalizedBuffValue !== -1) {
			return [buffId, normalizedBuffValue];
		}
		return null;
	};

	/**
	 * Converts normalized buff values back into what should be displayed on the card
	 * @param riven Riven entry from the Riven DB
	 * @param disposition Disposition of the weapon (OmegaAttenuation)
	 * @param buffId Id for the buff
	 * @param buffVal The displayed number for this buff on the riven
	 * @param numBuffs Total number of buffs on the riven
	 * @param modRank The current rank of the riven
	 * @param hasCurse Whether the riven has a curse
	 * @param isCurse Whether the value should be treated as a curse
	 */
	public static GetDisplayedBuffValue = (
		riven: Item<RivenData>,
		disposition: number,
		buffId: number,
		buffVal: number,
		numBuffs: number,
		modRank: number,
		hasCurse: boolean,
		isCurse: boolean,
	): number => {
		const baseBuffVal = GetRivenBaseBuffValue(disposition, numBuffs, hasCurse, isCurse);
		const displayedBuffValue = riven.data.UpgradeEntries.reduce(
			(result, rivenUpgrade): number => {
				if (rivenUpgrade.__id === buffId) {
					const upgrade = rivenUpgrade.Upgrades[0];
					if (!upgrade) {
						// Invalid data!
						return 1;
					}
					let buffMultiplier = upgrade.Upgrade.Value;
					if (isCurse) {
						buffMultiplier *= -1;
					}
					const reverseValueSymbol = upgrade.Schema.ReverseValueSymbol === 1;
					if (reverseValueSymbol) {
						buffMultiplier *= -1;
					}
					if (GetDisplayAsPercent(upgrade.Schema)) {
						buffMultiplier *= 100;
					}
					const buffRange = baseBuffVal * buffMultiplier * 0.2;
					const minBuffVal = baseBuffVal * buffMultiplier * 0.9;
					let displayedBuffValue = (minBuffVal + buffVal * buffRange) * (modRank + 1);

					// Add 1 to the value if it's a multiplier (e.g. 0.5 -> x1.5 Damage to Corpus)
					if (upgrade.Schema.DisplayAsMultiplier === 1) {
						displayedBuffValue += 1;
					}

					result = parseFloat(displayedBuffValue.toFixed(2));
				}
				return result;
			},
			-1,
		);
		if (displayedBuffValue !== -1) {
			return displayedBuffValue;
		}
		return 0;
	};
}
