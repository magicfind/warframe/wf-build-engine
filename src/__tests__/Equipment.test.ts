import {Equipment} from "..";
import {Vulklok} from "./fixtures";

test("Equipment.getWeaponSpread()", () => {
	expect(Equipment.getWeaponSpread(Vulklok, 0)).toBe(1);
});
