import {ArtifactPolarity} from "..";
import Mod from "../Mod";
import {
	Adaptation,
	AmalgamBarrelDiffusion,
	AnimalInstinct,
	ArcaneAcceleration,
	Ash,
	AssaultMode,
	BlindJustice,
	ChesaKubrow,
	CorrosiveProjection,
	Djinn,
	FatalAttraction,
	MaimingStrike,
	SteelCharge,
	UmbralVitality,
	Vulklok,
} from "./fixtures";

test("Mod.isAmalgam()", () => {
	expect(Mod.isAmalgam(AmalgamBarrelDiffusion)).toBe(true);
	expect(Mod.isAmalgam(CorrosiveProjection)).toBe(false);
});

test("Mod.isArcane()", () => {
	expect(Mod.isArcane(ArcaneAcceleration)).toBe(true);
	expect(Mod.isArcane(AmalgamBarrelDiffusion)).toBe(false);
});

test("Arcane drain should always be zero", () => {
	expect(Mod.getBaseDrain(ArcaneAcceleration)).toBe(0);
	expect(Mod.getDrain(ArcaneAcceleration, 0, ArtifactPolarity.AP_UNIVERSAL)).toBe(0);
	expect(Mod.getDrain(ArcaneAcceleration, 1, ArtifactPolarity.AP_UNIVERSAL)).toBe(0);
	expect(Mod.getDrain(ArcaneAcceleration, 2, ArtifactPolarity.AP_UNIVERSAL)).toBe(0);
	expect(Mod.getDrain(ArcaneAcceleration, 3, ArtifactPolarity.AP_UNIVERSAL)).toBe(0);
	expect(Mod.getDrain(ArcaneAcceleration, 0, ArtifactPolarity.AP_ATTACK)).toBe(0);
	expect(Mod.getDrain(ArcaneAcceleration, 3, ArtifactPolarity.AP_ATTACK)).toBe(0);
	expect(Mod.getDrain(ArcaneAcceleration, 0, ArtifactPolarity.AP_ANY)).toBe(0);
	expect(Mod.getDrain(ArcaneAcceleration, 3, ArtifactPolarity.AP_ANY)).toBe(0);
});

test("Mod.isCompatibleWith()", () => {
	// Sentinel mod
	expect(Mod.isCompatibleWith(AssaultMode, Djinn)).toBe(true);
	expect(Mod.isCompatibleWith(AssaultMode, ChesaKubrow)).toBe(false);
	expect(Mod.isCompatibleWith(AssaultMode, Vulklok)).toBe(false);
	expect(Mod.isCompatibleWith(AssaultMode, Ash)).toBe(false);

	// Companion mod
	expect(Mod.isCompatibleWith(AnimalInstinct, Djinn)).toBe(true);
	expect(Mod.isCompatibleWith(AnimalInstinct, ChesaKubrow)).toBe(true);
	expect(Mod.isCompatibleWith(AnimalInstinct, Vulklok)).toBe(false);
	expect(Mod.isCompatibleWith(AnimalInstinct, Ash)).toBe(false);

	// Djinn-specific mod
	expect(Mod.isCompatibleWith(FatalAttraction, Djinn)).toBe(true);
	expect(Mod.isCompatibleWith(FatalAttraction, ChesaKubrow)).toBe(false);
	expect(Mod.isCompatibleWith(FatalAttraction, Vulklok)).toBe(false);
	expect(Mod.isCompatibleWith(FatalAttraction, Ash)).toBe(false);

	// Warframe mod
	expect(Mod.isCompatibleWith(Adaptation, Djinn)).toBe(false);
	expect(Mod.isCompatibleWith(Adaptation, Vulklok)).toBe(false);
});

test("Verify default mod ranks and drains", () => {
	// Default max rank for melee Stance mod
	expect(Mod.getMaxRank(BlindJustice)).toBe(3);
	// Default max rank for regular mod
	expect(Mod.getMaxRank(MaimingStrike)).toBe(5);

	expect(Mod.getMaxRank(UmbralVitality)).toBe(10);

	// Default max rank and drain of Aura mod
	expect(
		Mod.getDrain(SteelCharge, Mod.getMaxRank(SteelCharge), ArtifactPolarity.AP_ANY),
	).toBe(-18);
});
