import {RivenSlotData} from "./Build";
import {
	ArtifactPolarity,
	ELEMENTAL_DAMAGE_TYPES,
	ModUsability,
	ProductCategory,
	Quota,
	Rarity,
	SlotPolarity,
	SlotType,
} from "./enums";
import Equipment from "./Equipment";
import {
	EquipmentData,
	Item,
	LocTag,
	ModData,
	PackagePath,
	RivenData,
	UpgradeData,
} from "./interfaces";
import Riven from "./Riven";
import Scripts from "./Scripts";

type FormatArgs = string[] | {[key: string]: string};

// TODO: Make sure this rounding function matches the game
const roundDecimal = (value: number, decimalPlaces?: number) => {
	decimalPlaces = decimalPlaces || 0;
	if (decimalPlaces === 0) {
		return value.toFixed(0);
	}
	const multiplier = 1 / decimalPlaces;
	return Number.parseFloat(value.toFixed(Math.log10(multiplier))).toString();
};

export const formatUpgradeValue = (upgrade: UpgradeData, upgradeValue: number) => {
	let displayValue = upgradeValue;

	if (upgrade.DisplayAsPercent) {
		displayValue *= 100;
	} else if (upgrade.DisplayAsMultiplier) {
		displayValue += 1;
	}

	if (upgrade.ReverseValueSymbol) {
		displayValue *= -1;
	}

	const decimalPlaces = upgrade.RoundTo || 0;
	if (upgrade.RoundingMode && decimalPlaces !== 0) {
		if (upgrade.RoundingMode === "RM_ROUND") {
			displayValue = Math.round(displayValue / decimalPlaces) * decimalPlaces;
		} else if (upgrade.RoundingMode === "RM_CEILING") {
			displayValue = Math.ceil(displayValue / decimalPlaces) * decimalPlaces;
		} else if (upgrade.RoundingMode === "RM_FLOOR") {
			displayValue = Math.floor(displayValue / decimalPlaces) * decimalPlaces;
		}
	}

	const mathSymbol = upgrade.DisplayAsMultiplier ? "x" : displayValue >= 0 ? "+" : "";
	return `${mathSymbol}${roundDecimal(displayValue, decimalPlaces)}`;
};

const getSyndicatePowerTag = (syndicatePath: string) => {
	switch (syndicatePath) {
		case "/Lotus/Syndicates/CephalonSudaSyndicate":
			return "/Lotus/Language/Syndicates/CephalonPower";
		case "/Lotus/Syndicates/ArbitersSyndicate":
			return "/Lotus/Language/Syndicates/ArbitersPower";
		case "/Lotus/Syndicates/SteelMeridianSyndicate":
			return "/Lotus/Language/Syndicates/MeridianPower";
		case "/Lotus/Syndicates/NewLokaSyndicate":
			return "/Lotus/Language/Syndicates/NewLokaPower";
		case "/Lotus/Syndicates/PerrinSyndicate":
			return "/Lotus/Language/Syndicates/PerrinPower";
		case "/Lotus/Syndicates/RedVeilSyndicate":
		default:
			return "/Lotus/Language/Syndicates/RedVeilPower";
	}
};

export default class Mod {
	public static isArcane(mod: Item<ModData>) {
		return mod.data.SearchTags
			? mod.data.SearchTags[0] === "/Lotus/Language/Categories/ENHANCEMENTS"
			: false;
	}

	public static isMeleeArcane(mod: Item<ModData>) {
		return mod.parents.includes("/Lotus/Types/Game/MeleeCosmeticEnhancer");
	}

	public static isAmalgam(mod: Item<ModData>) {
		return !!mod.data.IsAmalgam;
	}

	public static isGalvanized(mod: Item<ModData>) {
		return !!mod.data.IsGalvanized;
	}

	public static isGrimoire(mod: Item<ModData>) {
		return !!mod.data.IsGrimoire;
	}

	public static isAura(mod: Item<ModData>) {
		return !!mod.data.TargetMode || !!mod.data.TargetType;
	}

	public static isExilus(mod: Item<ModData>) {
		return mod.data.IsUtility === 1;
	}

	public static isImmortal(mod: Item<ModData>) {
		return !!mod.data.IsImmortal;
	}

	public static isRiven(path: PackagePath) {
		return path.startsWith("/Lotus/Upgrades/Mods/Randomized/");
	}

	public static isStance(mod: Item<ModData>) {
		return !!mod.data.Finishers;
	}

	public static isSentinel(mod: Item<ModData>) {
		const compat = mod.data.ItemCompatibility;
		return (
			compat === "/Lotus/Types/Sentinels/SentinelPowerSuit" ||
			compat === "/Lotus/Types/Game/Pets/RoboticPetPowerSuit"
		);
	}

	public static isFlawed(mod: Item<ModData>) {
		return !!mod.data.IsStarter;
	}

	public static isAvionic(mod: Item<ModData>) {
		return Equipment.isA(mod, "/Lotus/Types/Game/CrewShip/CrewShipUpgrade");
	}

	public static isSetMod(mod: Item<ModData>) {
		return !!mod.data.ModSet;
	}

	public static hasPositiveDrain(mod: Item<ModData>) {
		return Mod.isAura(mod) || Mod.isStance(mod);
	}

	public static hasRequiredCompatibilityTag(
		mod: Item<ModData>,
		item: Item<EquipmentData>,
	) {
		if (!mod.data.CompatibilityTags) return true;
		for (const tag of mod.data.CompatibilityTags || []) {
			if (
				Equipment.hasCompatibilityTag(item, tag) ||
				(tag === "ZANUKA_MOD" && Equipment.isHound(item))
			) {
				return true;
			}
		}
		return false;
	}

	public static isIncompatible(mod: Item<ModData>, item: Item<EquipmentData>) {
		for (const tag of mod.data.IncompatibilityTags || []) {
			if (Equipment.hasCompatibilityTag(item, tag)) {
				return true;
			}
		}
		return false;
	}

	public static getCompatibilityTagOverride = (
		mod: Item<ModData>,
	): LocTag | undefined => {
		if (mod.data.ItemCompatibilityLocOverride) {
			return mod.data.ItemCompatibilityLocOverride;
		} else if (Mod.isAura(mod)) {
			return "/Lotus/Language/Menu/CategoryAura";
		}
	};

	public static getModSlotType(mod: Item<ModData>) {
		return Mod.isAura(mod)
			? SlotType.AURA
			: Mod.isExilus(mod)
			? SlotType.EXILUS
			: Mod.isStance(mod)
			? SlotType.STANCE
			: Mod.isArcane(mod)
			? SlotType.ARCANE
			: SlotType.NORMAL;
	}

	public static isCompatibleWithSlot(mod: Item<ModData>, slotType: SlotType) {
		const modSlotType = Mod.getModSlotType(mod);
		// Mod and slot type match
		if (modSlotType === slotType) {
			return true;
		}
		// Exilus can be placed in normal slots
		if (Mod.isExilus(mod) && slotType === SlotType.NORMAL) {
			return true;
		}
		return false;
	}

	public static getPolarity(mod: Item<ModData>, rivenData?: RivenSlotData) {
		if (rivenData && rivenData.polarity) {
			return rivenData.polarity;
		}
		if (!mod.data.ArtifactPolarity) {
			return ArtifactPolarity.AP_UNIVERSAL;
		}
		return ArtifactPolarity[mod.data.ArtifactPolarity];
	}

	public static getRarity(mod: Item<ModData>) {
		if (!mod.data.Rarity) {
			return Rarity.COMMON;
		}
		return Rarity[mod.data.Rarity];
	}

	public static getMaxRank(mod: Item<ModData>) {
		// Immortal mods shouldn't display ranks
		if (Mod.isImmortal(mod)) {
			return 0;
		}
		// Stances default to a max of 3 ranks instead of 5
		return {
			[Quota.QA_NONE]: 0,
			// Coolant Leak is the only mod with a FusionLimit of "QA_LOW" and has no ranks
			[Quota.QA_LOW]: 0,
			[Quota.QA_MEDIUM]: 3,
			[Quota.QA_HIGH]: 5,
			[Quota.QA_VERY_HIGH]: 10,
		}[Quota[mod.data.FusionLimit || (Mod.isStance(mod) ? "QA_MEDIUM" : "QA_HIGH")]];
	}

	public static getBaseDrain(mod: Item<ModData>) {
		return {
			[Quota.QA_NONE]: 0,
			[Quota.QA_LOW]: 2,
			[Quota.QA_MEDIUM]: 4,
			[Quota.QA_HIGH]: 6,
			[Quota.QA_VERY_HIGH]: 10,
		}[Quota[mod.data.BaseDrain || "QA_LOW"]];
	}

	public static getPolarityMatch(
		slotPolarity: ArtifactPolarity,
		modPolarity: ArtifactPolarity,
	) {
		if (slotPolarity === ArtifactPolarity.AP_ANY) {
			return SlotPolarity.MATCH;
		}
		if (
			slotPolarity === ArtifactPolarity.AP_UNIVERSAL ||
			modPolarity === ArtifactPolarity.AP_UNIVERSAL
		) {
			return SlotPolarity.NEUTRAL;
		}
		if (slotPolarity === modPolarity) {
			return SlotPolarity.MATCH;
		}
		return SlotPolarity.MISMATCH;
	}

	public static getPolarityMultiplier(
		mod: Item<ModData>,
		slotPolarity: ArtifactPolarity,
		rivenData?: RivenSlotData,
	) {
		const modPolarity = Mod.getPolarity(mod, rivenData);
		const slotPolarityMatch = Mod.getPolarityMatch(slotPolarity, modPolarity);
		if (slotPolarityMatch === SlotPolarity.NEUTRAL) {
			return 1;
		}
		if (slotPolarityMatch === SlotPolarity.MATCH) {
			return Mod.hasPositiveDrain(mod) ? 2.0 : 0.5;
		}
		return Mod.hasPositiveDrain(mod) ? 0.75 : 1.25;
	}

	public static getUsability(mod: Item<ModData>) {
		const pve =
			mod.data.AvailableOnPve !== undefined ? !!mod.data.AvailableOnPve : true;
		const pvp = !!mod.data.AvailableOnPvp;
		if (pve && pvp) {
			return ModUsability.UNIVERSAL;
		} else if (pvp) {
			return ModUsability.PVP_ONLY;
		} else if (pve) {
			return ModUsability.PVE_ONLY;
		}
		return ModUsability.INVALID;
	}

	public static getDrain(
		mod: Item<ModData>,
		rank: number,
		slotPolarity: ArtifactPolarity,
		rivenData?: RivenSlotData,
	) {
		if (Mod.getModSlotType(mod) === SlotType.ARCANE) {
			return 0;
		}
		const baseDrain = rank + Mod.getBaseDrain(mod);
		// Aura mods add capacity instead of using it:
		const drain = Math.round(
			Mod.getPolarityMultiplier(mod, slotPolarity, rivenData) * baseDrain,
		);
		return Mod.hasPositiveDrain(mod) ? -drain : drain;
	}

	public static getUpgradeDescriptionData(
		upgrade: UpgradeData,
		rank: number,
		upgrades: UpgradeData[],
		modSetMultiplier?: number,
	): [string, FormatArgs | null, boolean] | void {
		// Treat damage resistance as (1 - value), see /Lotus/Upgrades/Mods/PvPMods/Warframe/BlastResist
		// eg. 0.95 AVATAR_DAMAGE_TAKEN should display as 5% damage resistance
		if (
			upgrade.UpgradeType === "AVATAR_DAMAGE_TAKEN" &&
			// FIXME: Exception for Boreal set bonus displaying incorrectly
			upgrade.LocTag !== "/Lotus/Language/Upgrades/BorealSetBonus"
		) {
			upgrade = {
				...upgrade,
				Value: 1 - upgrade.Value,
			};
			// Handle scripted mod descriptions
			if (upgrade.OverrideLocalization) {
				upgrades = upgrades.map((upgrade) =>
					upgrade.UpgradeType === "AVATAR_DAMAGE_TAKEN"
						? {...upgrade, Value: 1 - upgrade.Value, DisplayAsPercent: 1}
						: upgrade,
				);
			}
		}
		if (upgrade.OverrideLocalization) {
			// Handle locscript override
			if (upgrade.LocTag && upgrade.LocKeyWordScript) {
				if (upgrade.LocKeyWordScript.Function) {
					const ref = `${upgrade.LocKeyWordScript.Script}:${upgrade.LocKeyWordScript.Function}`;
					const script = Scripts[ref];
					if (script) {
						const args = script(
							rank,
							upgrade.LocKeyWordScript,
							upgrade.RoundTo,
							upgrades,
						);
						return [upgrade.LocTag, args, false];
					} else {
						return [upgrade.LocTag, ["?"], false];
					}
				} else {
					return [
						upgrade.LocTag,
						{
							val: formatUpgradeValue(
								upgrade,
								upgrade.Value * (rank + 1) * (modSetMultiplier || 1),
							),
						},
						false,
					];
				}
			}
		} else if (upgrade.UpgradeType) {
			const value = upgrade.Value * (rank + 1);
			if (upgrade.UpgradeType === "WEAPON_SYNDICATE_POWER") {
				return [getSyndicatePowerTag(upgrade.UpgradeObject), {val: `+${value}`}, false];
			} else {
				return [
					"|val|% |tag|",
					{
						val: formatUpgradeValue(
							upgrade,
							upgrade.OperationType === "STACKING_MULTIPLY" ? value * 100 : value,
						),
						tag: Equipment.getUpgradeStatTag(upgrade.UpgradeType),
					},
					true,
				];
			}
		}
	}

	public static formatUpgradeValue = formatUpgradeValue;

	/**
	 * Returns a list of tuple data that describe the mod's description.
	 * The tuple looks like:
	 * - `tag`: A LocTag (or a raw string if overrideLocalization is true)
	 * - `args`: Arguments to format the final string with (array or object)
	 * - `overrideLocalization`: If true, `tag` is a raw string, not a LocTag.
	 * @param mod The mod to check.
	 * @param rank The mod's expected rank (changes the values).
	 */
	public static getDescriptionData(
		mod: Item<ModData>,
		rank: number,
		modSetNumEquipped?: number,
	) {
		const ret: Array<[string, FormatArgs | null, boolean]> = [];
		if (mod.data.LocalizeDescTag && !mod.data.ModSet) {
			if (
				!mod.data.Upgrades ||
				!mod.data.Upgrades.find(
					(upgrade) =>
						!!upgrade.OverrideLocalization &&
						upgrade.LocTag === mod.data.LocalizeDescTag,
				)
			) {
				ret.push([mod.data.LocalizeDescTag, null, false]);
			}
		}
		const modSetMultiplier = this.getModSetMultiplier(mod, modSetNumEquipped);
		if (
			mod.data.Upgrades &&
			(mod.data.PrependSubUpgradeLoc || !mod.data.SubUpgrade || !ret.length)
		) {
			for (const upgrade of mod.data.Upgrades) {
				const data = Mod.getUpgradeDescriptionData(
					upgrade,
					rank,
					mod.data.Upgrades,
					modSetMultiplier,
				);
				if (data) {
					ret.push(data);
				}
			}
		} else if (mod.data.SubUpgrade) {
			// TODO: Confirm that this works correctly with all SubUpgrade mods
			const data = Mod.getDescriptionData(
				{...mod, data: mod.data.SubUpgrade},
				rank,
				modSetNumEquipped,
			);
			if (data) {
				ret.push(...data);
			}
		} else if (mod.data.SubUpgrades) {
			// Check Archon Flow for an example of this
			for (const subUpgrade of mod.data.SubUpgrades) {
				const data = Mod.getDescriptionData(
					{...mod, data: subUpgrade},
					rank,
					modSetNumEquipped,
				);
				if (data) {
					ret.push(...data);
				}
			}
		}

		// Handle new Tome Invocation mod descriptions
		// FIXME: This is a hacky way to do this, but it works for now
		// If `MaxConditionalStacks` in mod data, and FormatArgs is an object, add STACK: MaxConditionalStacks value to the FormatArgs
		if (mod.data.MaxConditionalStacks) {
			for (const tuple of ret) {
				if (tuple[1] && typeof tuple[1] === "object" && !("STACK" in tuple[1])) {
					// @ts-ignore
					tuple[1].STACK = mod.data.MaxConditionalStacks.toString();
				}
			}
		}

		// If `UpgradeDuration` in mod data, and FormatArgs is an object, add DURATION: UpgradeDuration value to the FormatArgs
		if (mod.data.UpgradeDuration) {
			for (const tuple of ret) {
				if (tuple[1] && typeof tuple[1] === "object" && !("DURATION" in tuple[1])) {
					// @ts-ignore
					tuple[1].DURATION = mod.data.UpgradeDuration.toString();
				}
			}
		}

		return ret;
	}

	public static getEnhancementData(mod: Item<ModData>, rank: number) {
		const ret: {
			values: Array<[string, string, boolean]>;
			upgrades: Array<[string, FormatArgs | null, boolean]>;
		} = {values: [], upgrades: []};
		if (mod.data.ConditionTag) {
			ret.values.push([mod.data.ConditionTag, "CONDITION", false]);
		}
		if (mod.data.UpgradeChance) {
			// UpgradeChance value of 0 indicates it is not used
			if (mod.data.UpgradeChance > 0) {
				ret.values.push([
					`${roundDecimal(mod.data.UpgradeChance * 100 * (rank + 1), 1)}`,
					"CHANCE",
					true,
				]);
			}
		} else {
			// seems to default to 10% per rank if UpgradeChance is missing
			ret.values.push([`${10 * (rank + 1)}`, "CHANCE", true]);
		}
		if (mod.data.UpgradeDuration && mod.data.UpgradeDuration > 0) {
			ret.values.push([
				`${(
					mod.data.UpgradeDuration *
					(mod.data.DurationScalesWithRank === 0 ? 1 : rank + 1)
				).toFixed(1)}`,
				"DURATION",
				true,
			]);
		}
		if (mod.data.MaxConditionalStacks) {
			ret.values.push([`${Math.round(mod.data.MaxConditionalStacks)}`, "STACKS", true]);
		}

		if (!mod.data.Upgrades) {
			return ret;
		}

		mod.data.Upgrades.forEach((upgrade) => {
			const data = Mod.getUpgradeDescriptionData(upgrade, rank, mod.data.Upgrades!);
			if (data) {
				ret.upgrades.push(data);
			}
		});
		return ret;
	}

	// We subtract 2 from numEquipped to match index for ModSetValues,
	// which starts from 0 with the multiplier for having 2 mods equipped
	public static getModSetMultiplier(mod: Item<ModData>, numEquipped?: number) {
		if (
			numEquipped &&
			numEquipped > 1 &&
			mod.data.ModSetValues &&
			numEquipped <= mod.data.ModSetValues.length + 2
		) {
			return 1 + mod.data.ModSetValues[numEquipped - 2];
		}
		return 1;
	}

	public static getElementalDamageTypes(
		mod: Item<ModData>,
		applyConditionals?: boolean,
		rivenData?: RivenSlotData,
	) {
		// Handle riven elements by reading from the bottom up
		if (rivenData) {
			return Array.from(Riven.getPassiveUpgrades(mod as Item<RivenData>, rivenData, 1))
				.reverse()
				.map((u) => u.DamageType)
				.filter((dt) => ELEMENTAL_DAMAGE_TYPES.indexOf(dt) !== -1);
		}
		if (!mod.data.Upgrades || (mod.data.ConditionalUpgrades && !applyConditionals)) {
			return [];
		}
		return mod.data.Upgrades.map((u) => u.DamageType).filter(
			(dt) => ELEMENTAL_DAMAGE_TYPES.indexOf(dt) !== -1,
		);
	}

	// Returns an iterator of upgrades which are active
	public static *getPassiveUpgrades(
		mod: Item<ModData>,
		applyConditionals?: boolean,
	): IterableIterator<UpgradeData> {
		if (!applyConditionals && mod.data.ConditionalUpgrades) {
			return;
		}
		// Aura mods must target avatars
		if (
			mod.data.TargetType !== undefined &&
			mod.data.TargetType !== "/Lotus/Types/Player/TennoAvatar"
		) {
			return;
		}
		// SubUpgrades (Mostly used for Avionics)
		if (mod.data.SubUpgrade) {
			for (const upgrade of Mod.getPassiveUpgrades(
				{...mod, data: mod.data.SubUpgrade},
				applyConditionals,
			)) {
				yield upgrade;
			}
		}

		if (mod.data.SubUpgrades) {
			// Check Archon Flow for an example of this
			for (const subUpgrade of mod.data.SubUpgrades) {
				for (const upgrade of Mod.getPassiveUpgrades(
					{...mod, data: subUpgrade},
					applyConditionals,
				)) {
					yield upgrade;
				}
			}
		}

		if (!mod.data.Upgrades) {
			return;
		}
		// Treat Blood Rush, Split Flights, etc as if they have max potential stacks
		if (mod.data.MaxConditionalStacks) {
			if (applyConditionals) {
				for (let i = 0; i < mod.data.MaxConditionalStacks; i++) {
					for (const upgrade of mod.data.Upgrades) {
						// Ignore "SymbolFilter": "CC_SLIDING_PVP" making Maiming Strike count twice
						if (upgrade.SymbolFilter?.includes("PVP")) {
							continue;
						}
						if (!applyConditionals) {
							if (
								upgrade.SymbolFilter &&
								upgrade.UpgradeType !== "GAMEPLAY_FACTION_DAMAGE"
							) {
								continue;
							}
							// Ignore "heavy attack" bonus for Sacrificial Steel unless "apply conditionals" is active
							if (upgrade.ValidModifiers?.length) {
								continue;
							}
						}
						yield upgrade;
					}
				}
			}
			return;
		}
		for (const upgrade of mod.data.Upgrades) {
			// Ignore "SymbolFilter": "CC_SLIDING_PVP" making Maiming Strike count twice
			if (upgrade.SymbolFilter?.includes("PVP")) {
				continue;
			}
			if (!applyConditionals) {
				if (upgrade.SymbolFilter && upgrade.UpgradeType !== "GAMEPLAY_FACTION_DAMAGE") {
					continue;
				}
				// Ignore "heavy attack" bonus for Sacrificial Steel unless "apply conditionals" is active
				if (upgrade.ValidModifiers?.length) {
					continue;
				}
			}
			yield upgrade;
		}
	}

	public static isCompatibleWith(
		mod: Item<ModData>,
		item: Item<EquipmentData>,
		helminthAbility?: PackagePath,
	) {
		if (Mod.isIncompatible(mod, item)) {
			return false;
		}

		if (
			Equipment.isHound(item) &&
			(Equipment.isA(
				mod,
				"/Lotus/Types/Friendly/Pets/ZanukaPets/ZanukaPetPrecepts/ZanukaBaseHeadPrecept",
			) ||
				Equipment.isA(
					mod,
					"/Lotus/Types/Friendly/Pets/ZanukaPets/ZanukaPetPrecepts/ZanukaBaseLegsPrecept",
				) ||
				Equipment.isA(
					mod,
					"/Lotus/Types/Friendly/Pets/ZanukaPets/ZanukaPetPrecepts/ZanukaBaseTailPrecept",
				) ||
				mod.data.CompatibilityTags?.includes("ZANUKA_MOD") ||
				mod.data.ItemCompatibility === "/Lotus/Types/Game/SentinelPowerSuit")
		) {
			return true;
		}

		if (!Mod.hasRequiredCompatibilityTag(mod, item)) {
			return false;
		}

		if (mod.data.ItemCompatibility && Equipment.isA(item, mod.data.ItemCompatibility)) {
			return true;
		}

		// Zaw arcanes have "/Lotus/Weapons/Ostron/Melee/LotusModularWeapon" item compatibility tag but that tag is not included in their parents array.
		if (
			Equipment.isZaw(item) &&
			(mod.data.ItemCompatibility ===
				"/Lotus/Weapons/Ostron/Melee/LotusModularWeapon" ||
				(item.data.MeleeStyle &&
					mod.data.ModularWeaponMeleeStyleCompatibility === item.data.MeleeStyle))
		) {
			return true;
		}

		// Kitgun arcanes have a similar issue as zaw arcanes
		if (
			Equipment.isKitgun(item) &&
			mod.data.ItemCompatibility === "/Lotus/Weapons/Tenno/LotusBulletWeapon"
		) {
			return true;
		}

		if (
			mod.data.ItemCompatibility === "/Lotus/Weapons/Tenno/Pistol/LotusPistol" &&
			Equipment.isA(item, "/Lotus/Types/Weapon/LotusCustomAimWeapon")
		) {
			// Hardcoded check for Regulators Prime
			return true;
		}

		if (Mod.isSentinel(mod)) {
			return item.data.ProductCategory === ProductCategory.Sentinels;
		}

		// Allow augments for helminth abilities to be equipped
		if (
			helminthAbility &&
			mod.data.IsHelminthAugment &&
			mod.data.Upgrades &&
			mod.data.Upgrades.find((upgrade) => upgrade.UpgradeObject === helminthAbility)
		) {
			return true;
		}

		return false;
	}
}
